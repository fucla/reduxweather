import React, { Component } from 'react';
import { connect } from 'react-redux';
import Chart from '../components/chart';
import GoogleMap from '../components/google_map';

export default class WeatherList extends Component { // eslint-disable-line
  renderWeather(cityData) {
    const name = cityData.city.name;
    const temps = cityData.list.map(weather => weather.main.temp);
    const pressures = cityData.list.map(weather => weather.main.pressure);
    const humidities = cityData.list.map(weather => weather.main.humidity);
    // const lon = cityData.city.coord.lon;
    // const lat = cityData.city.coord.lat;
    // destructuring in ES6
    const { lon, lat } = cityData.city.coord;

    return (
      <tr key={ name }>
        <td><GoogleMap lon={ lon } lat={ lat } /></td>
        <td><Chart data={ temps } color="orange" units="K" /></td>
        <td><Chart data={ pressures } color="green" units="hPa" /></td>
        <td><Chart data={ humidities } color="black" units="%" /></td>
      </tr>
    );
  }

  render() {
    return (
      <table className="table table-hover">
        <thead>
          <tr>
            <th>City</th>
            <th>Temperature (K)</th>
            <th>Pressure (hPA)</th>
            <th>Humidity (%)</th>
          </tr>
        </thead>
        <tbody>
          { this.props.weather.map(this.renderWeather) }
        </tbody>
      </table>
    );
  }
}

WeatherList.propTypes = {
  weather: React.PropTypes.array,
  renderWeather: React.PropTypes.func,
};

function mapStateToProps({ weather }) {
  // const weather = state.weather
  return { weather };
}

export default connect(mapStateToProps)(WeatherList);
