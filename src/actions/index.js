import axios from 'axios';

const API_KEY = '4b355a70e0e8cf93c8c55d43689c2659';
// template string below
const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;

export const FETCH_WEATHER = 'FETCH_WEATHER';

/**
 * Middleware lets action pass, manipulates it, logs it, or stops it.
 */
export function fetchWeather(city) {
  const url = `${ROOT_URL}&q=${city},us`;
  const request = axios.get(url);

  // console.log('Request:', request);

  return {
    type: FETCH_WEATHER,
    payload: request,
  };
}
